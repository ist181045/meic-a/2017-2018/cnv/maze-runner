/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.InstanceMetrics;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.InstanceManager;

public class RegisterHandler implements HttpHandler {

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    if(exchange.getRequestMethod().equals("POST")) {
      BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getRequestBody()));

      String input = reader.readLine();
      String instanceEndpoint = input.substring(0, input.indexOf(";"));
      String instanceId = input.substring(input.indexOf(";") + 1);
      if(instanceEndpoint.equals("") || instanceId.equals("")) {
        exchange.sendResponseHeaders(400, -1);
        exchange.close();
        return;
      }
      InstanceManager instance = InstanceManager.getInstance();
      String instanceName = instanceEndpoint.substring(0, instanceEndpoint.indexOf(":"));
      String instancePort = instanceEndpoint.substring(instanceEndpoint.indexOf(":") + 1);
      instance.addInstance(instanceName, new InstanceMetrics(instanceName, instanceId,
          instancePort));

      InstanceManager.getInstance().setLastUpdate(new Date(System.currentTimeMillis()));
      System.out.println(String.format("[%tT] Registered EC2 instance with name %s and id %s running on port %s", System.currentTimeMillis(), instanceName, instanceId, instancePort));
      exchange.sendResponseHeaders(200, -1);
    }
    else {
      exchange.sendResponseHeaders(405, 0);
    }
    exchange.close();
  }
}
