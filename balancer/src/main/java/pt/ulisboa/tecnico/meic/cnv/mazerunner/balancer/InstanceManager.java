/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.InstanceMetrics;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.ThreadMetrics;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.averages.Parameters;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.averages.Results;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.runnables.AutoscaleTask;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.runnables.GetUpdateTask;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.runnables.HealthCheckTask;

public class InstanceManager {

  private static InstanceManager instance;

  private Map<String, InstanceMetrics> currentInstances = new ConcurrentHashMap<>();
  private Map<Parameters, Results> currentAverages = new ConcurrentHashMap<>();
  private long maxCalls = Integer.MAX_VALUE;

  private List<String> stoppedInstances = new ArrayList<>();
  private List<String> toBeRemoved = new ArrayList<>();
  private Date lastUpdate = new Date(System.currentTimeMillis());

  private static Timer timer;
  private static Timer healthCheck;
  private static Timer autoscaler;
  private static final int HEALTH_CHECK_PERIOD = 1000 * 5; //5 seconds
  private static final int UPDATE_PERIOD = 1000 * 10; //10 seconds
  private static final int AUTOSCALE_PERIOD = 1000 * 30; //30 seconds


  private InstanceManager() {
  }

  public static InstanceManager getInstance() {
    if (instance == null) {
      instance = new InstanceManager();
      timer = new Timer();
      GetUpdateTask task = new GetUpdateTask();
      timer.schedule(task, 0, UPDATE_PERIOD);

      healthCheck = new Timer();
      healthCheck.schedule(new HealthCheckTask(), 0, HEALTH_CHECK_PERIOD);

      autoscaler = new Timer();
      autoscaler.schedule(new AutoscaleTask(), AUTOSCALE_PERIOD, AUTOSCALE_PERIOD);
    }
    return instance;
  }

  public Map<String, InstanceMetrics> getCurrentInstances() {
    return currentInstances;
  }

  public void addInstance(String instanceName, InstanceMetrics instance) {
    currentInstances.put(instanceName, instance);
  }

  public Map<Parameters, Results> getCurrentAverages() {
    return currentAverages;
  }

  public List<String> getStoppedInstances() {
    return stoppedInstances;
  }

  public Date getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(Date lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public synchronized long getMaxCalls() {
    return maxCalls;
  }

  public void setMaxCalls(long newMaxCalls) {
    maxCalls = newMaxCalls;
  }



  private synchronized String getLessLoadedInstance() {
    Map<String, Long> instances = new HashMap<>();

    System.out.println();
    for (Entry<String, InstanceMetrics> ec2Instance : currentInstances.entrySet()) {
      if(!ec2Instance.getValue().isHealthy() || ec2Instance.getValue().isToBeRemoved()) { //remove instances considered unhealthy from considerations
        continue;
      }

      long computationLeft = 0;
      for (Entry<String, ThreadMetrics> thread : ec2Instance.getValue()
          .getCurrentThreads().entrySet()) {
        Parameters searchParams = new Parameters(thread.getValue());
        Results result = currentAverages.get(searchParams);
        if (result == null) {
          if(computationLeft >= Integer.MAX_VALUE) {
            computationLeft++;
          }
          else if(maxCalls == Integer.MAX_VALUE ) {
            computationLeft = Integer.MAX_VALUE;
          }
          else {
            if(thread.getValue().getMethodCalls() > maxCalls) {
              computationLeft = Integer.MAX_VALUE;
            }
            else {
              computationLeft += maxCalls;
            }
          }
        } else {
          computationLeft += Math
              .max(0, result.getMethodCalls() - thread.getValue().getMethodCalls()) / thread.getValue().getSpeed();
        }
      }
      instances.put(ec2Instance.getKey(), computationLeft);
    }
    System.out.println();

    return instances.entrySet().stream()
        .min((o1, o2) -> o1.getValue() < o2.getValue() ? -1 : 1)
        .map(Entry::getKey)
        .orElse(null);
  }

  public synchronized String decideNextRequest(ThreadMetrics metrics) {

    String selectedInstance = getLessLoadedInstance();

    //update data object straight away to prevent having to wait for the instance to register the thread in Dynamo
    currentInstances.get(selectedInstance).getCurrentThreads().put("", metrics);

    return selectedInstance + ":" + currentInstances.get(selectedInstance).getPort();
  }

  public synchronized void markInstanceForRemoval() {
    String instance = getLessLoadedInstance();
    if(instance != null) {
      currentInstances.get(instance).setToBeRemoved(true);
      toBeRemoved.add(instance);
      System.out.println(String.format("[%tT] Instance %s has been marked for removal", System.currentTimeMillis(), instance));
    }
  }

  public void removeInstance(String instanceId) {
    currentInstances.remove(instanceId);
    stoppedInstances.add(instanceId);
    System.out.println(String.format("[%tT] Instance %s has been removed", System.currentTimeMillis(), instanceId));
  }

  public List<String> getToBeRemoved() {
    return toBeRemoved;
  }
}
