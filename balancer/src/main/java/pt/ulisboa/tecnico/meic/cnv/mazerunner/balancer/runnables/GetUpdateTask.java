/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.runnables;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimerTask;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.InstanceManager;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.InstanceMetrics;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.ThreadMetrics;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.averages.Parameters;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.averages.Results;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.util.ThreadMetricsConverter;

public class GetUpdateTask extends TimerTask {

  public static final String TABLE_NAME = "cnv-mazeruner-metrics";
  private static DynamoDB dynamo = new DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(
      Regions.EU_WEST_2).withCredentials(new ProfileCredentialsProvider()).build());

  @Override
  public void run() {
    InstanceManager instance = InstanceManager.getInstance();
    for (Entry<String, InstanceMetrics> element:
        instance.getCurrentInstances().entrySet()){
      if(element.getValue().isHealthy()) {
        updateInstanceMetrics(element.getKey());
      }
    }
  }

  private void updateInstanceMetrics(String instanceName) {
    Table t = dynamo.getTable(TABLE_NAME);
    GetItemSpec spec = new GetItemSpec().withPrimaryKey("instance_id", instanceName);
    Item i = t.getItem(spec);
    if(i == null) {
      //TODO: remove item from list?
    }
    else {
      //Update currently running threads
      InstanceManager instance = InstanceManager.getInstance();
      Map<String, Object> threads = i.getMap("threads");

      ThreadMetricsConverter converter = new ThreadMetricsConverter();


      //get threads currently running
      Map<String, ThreadMetrics> metricsList = new HashMap<>();
      for (Entry<String, Object> element : threads.entrySet()){
        ThreadMetrics threadMetrics = converter.unconvert(((Map<String, Object>) element.getValue()));
        metricsList.put(element.getKey(), threadMetrics);
      }
      InstanceMetrics instanceMetrics = InstanceManager.getInstance().getCurrentInstances()
          .get(instanceName);
      if(instanceMetrics != null) {
        instanceMetrics.setCurrentThreads(metricsList);
      }


      //update averages
      List<Object> done = i.getList("completed");
      for (Object o : done) {
        ThreadMetrics threadMetrics = converter.unconvert(((Map<String, Object>) o));
        Parameters params = new Parameters(threadMetrics);
        Results res = instance.getCurrentAverages().get(params);
        if(res == null) {
          instance.getCurrentAverages().put(params, new Results(threadMetrics.getMethodCalls(), 1));
        }
        else {
          long newAverage = ((res.getMethodCalls() * res.getOccurences()) + threadMetrics.getMethodCalls()) / (res.getOccurences() + 1);
          instance.getCurrentAverages().put(params, new Results(newAverage, res.getOccurences() + 1));
        }
        if(threadMetrics.getMethodCalls() > instance.getMaxCalls() || instance.getMaxCalls() == Integer.MAX_VALUE) {
          System.out.println("New maximum method count achieved: " + threadMetrics.getMethodCalls() + "!");
          instance.setMaxCalls(threadMetrics.getMethodCalls());
        }
      }

      //delete already processed completed results
      String updateExpression = "SET #C = :val1";
      Map<String, String> expressionAttributeNames = new HashMap<>();
      Map<String, Object> expressionAttributeValues = new HashMap<>();
      expressionAttributeNames.put("#C", "completed");
      expressionAttributeValues.put(":val1", new ArrayList<>());
      try {
        UpdateItemOutcome out = t.updateItem("instance_id", instanceName, updateExpression, expressionAttributeNames, expressionAttributeValues);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
