# Maze Runner
### Cloud Computing and Virtualization Project - 17'18

---

### Problem Description

The goal of the project is to design and develop an elastic cluster of web
servers that is able to execute a simple gaming function: to solve escape paths
from labyrinths (mazes), on-demand, by executing a set of search/exploration
algorithms (serving as a demonstrator of CPU-intensive processing).

The system will receive a stream of web requests from users. Each request is for
the solving of the escape path of a given maze, providing the coordinates of the
initial position (entry) and the location of the escape door (exit), and in the
end displaying the maze and the escape path to the user.

Each request can result in a task of varying complexity to process, as different
search algorithms will take different number of steps to solve the escape path,
taking into account different maze configurations, maze size, entry and exit
coordinates, and simulating different thinking speed/delay.

To have scalability, good performance and efficiency, the system will have to
optimize the selection of the cluster node for each incoming request and to
optimize the number of active nodes in the cluster.

---

More information in the [Problem Statement](statement.pdf)
