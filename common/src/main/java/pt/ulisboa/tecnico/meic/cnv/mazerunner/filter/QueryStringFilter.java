/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.filter;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.util.Exchanges;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.util.Uris;

public class QueryStringFilter extends Filter {

  public static final String ATTRIBUTE_QUERY_MAP = "QUERY_MAP";

  @Override
  public void doFilter(HttpExchange exchange, Chain chain) throws IOException {
    String queryString = exchange.getRequestURI().getQuery();
    try {
      exchange.setAttribute(ATTRIBUTE_QUERY_MAP, Uris.parseQuery(queryString));
      chain.doFilter(exchange);
    } catch (IllegalArgumentException e) {
      Exchanges.sendError(400, exchange, e.getMessage());
    }
  }

  @Override
  public String description() {
    return String.format("Parses the query, returning a map accessible via the %s attribute",
        ATTRIBUTE_QUERY_MAP);
  }
}
