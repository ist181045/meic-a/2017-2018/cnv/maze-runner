package pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.strategies;

import pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.Maze;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.exceptions.InvalidCoordinatesException;

public abstract class MazeRunningStrategy {

  public final void solve(Maze maze, int xStart, int yStart, int xFinal, int yFinal, int velocity)
      throws InvalidCoordinatesException {
    boolean isStartWall = maze.isWall(xStart, yStart);
    boolean isFinalWall = maze.isWall(xFinal, yFinal);

    if (isStartWall || isFinalWall) {
      String exceptionMessage = "";
      if (isStartWall) {
        exceptionMessage += "Start Point Invalid, suggestions: ";
        exceptionMessage = buildSuggestions(maze, xStart, yStart, exceptionMessage);
        exceptionMessage += System.lineSeparator();
      }
      if (isFinalWall) {
        exceptionMessage += "Final Point Invalid, suggestions: ";
        exceptionMessage = buildSuggestions(maze, xFinal, yFinal, exceptionMessage);
      }
      throw new InvalidCoordinatesException(exceptionMessage);
    }

    run(maze, xStart, yStart, xFinal, yFinal, velocity);

    maze.setPos(xStart, yStart, Maze.INITIAL_CHAR);
    maze.setPos(xFinal, yFinal, Maze.FINAL_CHAR);
  }

  private String buildSuggestions(Maze maze, int xFinal, int yFinal, String exceptionMessage) {
    StringBuilder sb = new StringBuilder(exceptionMessage);
    for (int x = xFinal - 1; x <= xFinal + 1; x++) {
      for (int y = yFinal - 1; y <= yFinal + 1; y++) {
        if (x == xFinal && y == yFinal) {
          continue;
        }
        if (!maze.isWall(x, y)) {
          sb.append("(").append(x).append(",").append(y).append(") | ");
        }
      }
    }
    exceptionMessage = sb.toString();
    return exceptionMessage;
  }

  public abstract void run(Maze maze, int xStart, int yStart, int xFinal, int yFinal, int velocity)
      throws InvalidCoordinatesException;

}
