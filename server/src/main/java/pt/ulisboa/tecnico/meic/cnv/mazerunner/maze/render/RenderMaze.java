package pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.render;

import pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.Maze;

public interface RenderMaze {

  String render(Maze maze, int velocity);

}
