/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.server.runnables;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimerTask;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.domain.ThreadMetrics;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.server.Metrics;

public class UpdateTask extends TimerTask {

  // private static final String FILE_PATH = System.getProperty("user.dir") + File.separatorChar
  // + "outputs";
  public static final String TABLE_NAME = "cnv-mazeruner-metrics";
  private static DynamoDB dynamo = new DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(
      Regions.EU_WEST_2).withCredentials(new ProfileCredentialsProvider()).build());

  @Override
  public void run() {
    Metrics metrics = Metrics.getInstance();
    for (Entry<Long, ThreadMetrics> element :
        metrics.getThreads().entrySet()) {

      //single thread handles all the requests to Dynamo
      writeData(element.getKey(), element.getValue().getMethodCalls(),
          element.getValue().getStrategy(), element.getValue().getMaze(),
          element.getValue().getDistance(), element.getValue().getSpeed());
    }
  }

  public static void writeData(long thread, long methodCalls, String strategy, String maze,
      int distance, int speed) {
    String instanceName = Metrics.getInstanceName();
    Table t = dynamo.getTable(TABLE_NAME);
    GetItemSpec spec = new GetItemSpec().withPrimaryKey("instance_id", instanceName);
    Item i = t.getItem(spec);
    if (i == null) {
      Map<String, Map> threadList = new HashMap<>();
      Map<String, Object> threadInfo = new HashMap<>();
      threadInfo.put("methodCalls", methodCalls);
      threadInfo.put("strategy", strategy);
      threadInfo.put("maze", maze);
      threadInfo.put("distance", distance);
      threadInfo.put("speed", speed);
      threadList.put(String.valueOf(thread), threadInfo);
      Item item = new Item().withPrimaryKey("instance_id", instanceName)
          .withMap("threads", threadList).withList("completed", new ArrayList<>());
      t.putItem(item);
    } else {
      Map<String, Object> currentThreads = i.getMap("threads");
      String updateExpression;
      Map<String, String> expressionAttributeNames = new HashMap<>();
      Map<String, Object> expressionAttributeValues = new HashMap<>();

      if (currentThreads.get(String.valueOf(thread)) != null) {
        updateExpression = "SET threads.#T.methodCalls = :val1";
        expressionAttributeNames.put("#T", String.valueOf(thread));
        expressionAttributeValues.put(":val1", methodCalls);
      } else {
        Map<String, Object> threadInfo = new HashMap<>();
        threadInfo.put("methodCalls", methodCalls);
        threadInfo.put("strategy", strategy);
        threadInfo.put("maze", maze);
        threadInfo.put("distance", distance);
        threadInfo.put("speed", speed);
        updateExpression = "SET threads.#T = :val1";
        expressionAttributeNames.put("#T", String.valueOf(thread));
        expressionAttributeValues.put(":val1", threadInfo);
      }
      UpdateItemOutcome out = t
          .updateItem("instance_id", instanceName, updateExpression, expressionAttributeNames,
              expressionAttributeValues);
      //TODO: verify output of operation
    }
    /*String update;
    try {
      if(!created) {
        File f = new File(FILE_PATH);
        created = f.mkdir();
        update = String.format("Thread %d is running strategy %s\n%d methods called\n", thread,
        strategy, instructions);
        System.out.print(update);
        Files.write(Paths.get(FILE_PATH + File.separatorChar + parentId + ".txt"), update
        .getBytes(),
            StandardOpenOption.CREATE);
      }
      else {
        update = String.format("%d methods called\n", instructions);
        System.out.print(update);
        Files.write(Paths.get(FILE_PATH + File.separatorChar + parentId + ".txt"), update
        .getBytes(),
            StandardOpenOption.APPEND);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }*/
  }

  public static void removeThread(long parentId, ThreadMetrics thread) {
    String instanceName = Metrics.getInstanceName();
    Table t = dynamo.getTable(TABLE_NAME);
    Map<String, Object> threadInfo = new HashMap<>();
    threadInfo.put("methodCalls", thread.getMethodCalls());
    threadInfo.put("strategy", thread.getStrategy());
    threadInfo.put("maze", thread.getMaze());
    threadInfo.put("distance", thread.getDistance());
    threadInfo.put("speed", thread.getSpeed());

    String updateExpression = "REMOVE threads.#T SET #C = list_append(#C, :val1)";
    Map<String, String> expressionAttributeNames = new HashMap<>();
    Map<String, Object> expressionAttributeValues = new HashMap<>();
    expressionAttributeNames.put("#T", String.valueOf(parentId));
    expressionAttributeNames.put("#C", "completed");
    List<Object> threadList = new ArrayList<>();
    threadList.add(threadInfo);
    expressionAttributeValues.put(":val1", threadList);

    try {
      UpdateItemOutcome out = t
          .updateItem("instance_id", instanceName, updateExpression, expressionAttributeNames,
              expressionAttributeValues);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}
