/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.server.handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.filter.QueryStringFilter;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.Main;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.exceptions.CantGenerateOutputFileException;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.exceptions.CantReadMazeInputFileException;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.exceptions.InvalidCoordinatesException;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.exceptions.InvalidMazeRunningStrategyException;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.server.Metrics;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.util.Exchanges;

public class RunnerHandler implements HttpHandler {

  private static final String[] MAZE_ARGS = {"m", "x0", "y0", "x1", "y1", "v", "s"};

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    @SuppressWarnings("unchecked")
    Map<String, String> query = (Map<String, String>) exchange
        .getAttribute(QueryStringFilter.ATTRIBUTE_QUERY_MAP);
    String mazeFile = query.get("m");
    String maze = mazeFile.substring(0, mazeFile.lastIndexOf('.')).replace(".", "-");
    String solution = String.format("%s-%d.html", maze, System.nanoTime());

    if (query.keySet().containsAll(Arrays.asList(MAZE_ARGS))) {
      String[] args = {
          query.get("x0"), query.get("y0"), query.get("x1"), query.get("y1"), query.get("v"),
          query.get("s"), mazeFile, solution
      };

      try {
        Metrics metrics = Metrics.getInstance();
        int distance = ((int) Math.round(
            Math.hypot(Double.valueOf(query.get("x0")) - Double.valueOf(query.get("x1")),
                Double.valueOf(query.get("y0")) - Double.valueOf(query.get("y1")))));
        metrics.addRunner(Thread.currentThread().getId(), 0, args[5], mazeFile, distance, Integer.valueOf(args[4]));
        Main.main(args);

        Path path = Paths.get(solution);
        List<String> lines = Files.readAllLines(path, Charset.defaultCharset());
        StringBuilder responseBuilder = new StringBuilder();

        for (String line : lines) {
          responseBuilder.append(line).append(System.lineSeparator());
        }

        Exchanges.sendResponse(200, exchange, responseBuilder.toString());
      } catch (InvalidMazeRunningStrategyException | CantReadMazeInputFileException
          | CantGenerateOutputFileException | InvalidPathException | IOException se) {
        // Server side error
        Exchanges.sendError(500, exchange, se.getMessage());
      } catch (InvalidCoordinatesException ce) {
        // Client side error
        Exchanges.sendError(400, exchange, ce.getMessage());
      }
    } else {
      StringBuilder responseBuilder = new StringBuilder("Missing arguments: ");

      for (int i = 0; i < MAZE_ARGS.length; i++) {
        String arg = MAZE_ARGS[i];
        if (!query.containsKey(arg)) {
          if (i > 0) {
            responseBuilder.append(" ");
          }
          responseBuilder.append(arg);
        }
      }
      Exchanges.sendError(400, exchange, responseBuilder.toString());
    }
  }
}
