/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.server;

import com.amazonaws.util.EC2MetadataUtils;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.domain.ThreadMetrics;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.server.runnables.UpdateTask;

public class Metrics {

  private static Metrics instance;
  private Timer timer;
  private Map<Long, ThreadMetrics> threads = new ConcurrentHashMap<>();
  private static final String INSTANCE_NAME = (EC2MetadataUtils.getLocalHostName() == null ? "localhost" : EC2MetadataUtils.getLocalHostName());
  private static final long UPDATE_PERIOD = 1000 * 10; //10 seconds

  private Metrics() {
    Timer ti = new Timer();
    UpdateTask task = new UpdateTask();
    timer = ti;
    ti.schedule(task, UPDATE_PERIOD, UPDATE_PERIOD);
  }

  public static Metrics getInstance() {
    if(instance == null) {
      instance = new Metrics();
    }
    return instance;
  }

  public Map<Long, ThreadMetrics> getThreads() {
    return threads;
  }

  public static String getInstanceName() {
    return INSTANCE_NAME;
  }

  public void addRunner(long parentId, long inst, String strat, String maze, int distance, int speed) {
    String newMaze = maze.substring(0, maze.indexOf('.'));
    ThreadMetrics th = new ThreadMetrics(strat, inst, newMaze, distance, speed);
    threads.put(parentId, th);
    UpdateTask.writeData(parentId, inst, strat, newMaze, distance, speed);
  }

  public void removeRunner(long parentId) {
    ThreadMetrics thread = threads.get(parentId);
    threads.remove(parentId);
    UpdateTask.removeThread(parentId, thread);
  }

  public void incrRunner(long parentId) {
    if(threads.get(parentId) == null) {
      return;
    }
    threads.get(parentId).incrementCount();
  }
}
