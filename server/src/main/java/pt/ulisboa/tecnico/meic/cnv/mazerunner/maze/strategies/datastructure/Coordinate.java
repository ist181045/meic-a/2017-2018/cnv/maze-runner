package pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.strategies.datastructure;

import java.util.LinkedList;
import java.util.List;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.maze.Maze;

public class Coordinate {

  private final int x, y;

  public Coordinate(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return this.x;
  }

  public int getY() {
    return this.y;
  }

  public List<Coordinate> getAllNeighbours(Maze maze) {
    List<Coordinate> neighbours = new LinkedList<>();
    if (maze.isUnvisitedPassage(x - 1, y) || maze.isVisitedPassage(x - 1, y)) {
      neighbours.add(new Coordinate(x - 1, y));
    }
    if (maze.isUnvisitedPassage(x + 1, y) || maze.isVisitedPassage(x + 1, y)) {
      neighbours.add(new Coordinate(x + 1, y));
    }
    if (maze.isUnvisitedPassage(x, y - 1) || maze.isVisitedPassage(x, y - 1)) {
      neighbours.add(new Coordinate(x, y - 1));
    }
    if (maze.isUnvisitedPassage(x, y + 1) || maze.isVisitedPassage(x, y + 1)) {
      neighbours.add(new Coordinate(x, y + 1));
    }
    return neighbours;
  }

  public List<Coordinate> getUnvisitedNeighbours(Maze maze) {
    List<Coordinate> neighbours = new LinkedList<>();
    if (maze.isUnvisitedPassage(x - 1, y)) {
      neighbours.add(new Coordinate(x - 1, y));
    }
    if (maze.isUnvisitedPassage(x + 1, y)) {
      neighbours.add(new Coordinate(x + 1, y));
    }
    if (maze.isUnvisitedPassage(x, y - 1)) {
      neighbours.add(new Coordinate(x, y - 1));
    }
    if (maze.isUnvisitedPassage(x, y + 1)) {
      neighbours.add(new Coordinate(x, y + 1));
    }
    return neighbours;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    Coordinate c = (Coordinate) obj;
    return x == c.x && y == c.y;
  }

  @Override
  public int hashCode() {
    return x - y;
  }
}
