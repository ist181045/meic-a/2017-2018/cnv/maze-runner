/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import BIT.highBIT.ClassInfo;
import BIT.highBIT.Routine;
import java.io.File;
import java.io.FileFilter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.domain.ThreadMetrics;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.server.Metrics;

public class MethodCount {

  private static final FileFilter RECURSIVE_CLASS_FILE_FILTER = new FileFilter() {
    @Override
    public boolean accept(File file) {
      return file.getName().endsWith(".class") || file.isDirectory();
    }
  };

  private static final String CLASS_NAME = MethodCount.class.getSimpleName();


  public static void main(String[] args) {
    if (args.length > 0) {
      File file = Paths.get(args[0]).toFile();
      instrument(file);
    } else {
      throw new IllegalArgumentException("Missing path to class files to instrument");
    }
  }

  @SuppressWarnings("unchecked")
  private static void instrument(File file) {
    if (file.isDirectory()) {
      File[] children = file.listFiles(RECURSIVE_CLASS_FILE_FILTER);
      for (File child : Objects.requireNonNull(children)) {
        instrument(child);
      }
    } else {
      String path = file.getAbsolutePath();
      ClassInfo ci = new ClassInfo(path);
      ArrayList<Routine> routines = Collections.list(ci.getRoutines().elements());
      /*String className = ci.getClassName();
      int abs = ci.getClassFile().access_flags & Constant.ACC_ABSTRACT;
      if (className.endsWith("Strategy") && abs == 0) {
        ci.addBefore(CLASS_NAME, "strategy", className.substring(className.lastIndexOf('/') + 1));
      }*/

      for (Routine routine : routines) {
        routine.addBefore(CLASS_NAME, "count", 0);
      }
      ci.addAfter(CLASS_NAME, "printCount", 0);
      ci.write(path);
    }
  }

  @SuppressWarnings("unused")
  public static void count(int unused) {
    Metrics metrics = Metrics.getInstance();
    long thread = Thread.currentThread().getId();
    metrics.incrRunner(thread);
  }

  @SuppressWarnings("unused")
  public static void printCount(int unused) {
    Metrics metrics = Metrics.getInstance();
    long thread = Thread.currentThread().getId();
    ThreadMetrics threadMetrics = metrics.getThreads().get(thread);
    System.out.println("Method count: " + threadMetrics.getMethodCalls());
    System.out.println("Strategy employed: " + threadMetrics.getStrategy());
    System.out.println("Maze traversed: " + threadMetrics.getMaze());
    System.out.println("Distance: " + threadMetrics.getDistance());
    System.out.println("Speed: " + threadMetrics.getSpeed());
    metrics.removeRunner(thread);
  }

}
